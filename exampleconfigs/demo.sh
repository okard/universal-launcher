#!/usr/bin/env bash

while :
do
    TIMESTAMP=`date '+%H:%M:%S'`
	echo "$TIMESTAMP > Hello World! $1"
	sleep 1
done
