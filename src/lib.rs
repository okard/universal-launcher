
// liblaunch

#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate toml;
extern crate regex;

mod model;
mod engine;

use std::path::Path;

pub use engine::Engine;

//TODO logging

//helper to resolve path values

pub fn load_file<P: AsRef<Path>>(file_path: P) -> Result<model::LauncherConfig, &'static str> //TODO better error type
{
    use std::fs::File;
    use std::io::Read;
    let mut file = File::open(file_path).map_err(|_|"Failed to read file")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents).map_err(|_|"Failed to read file to string")?;
    let decoded: model::LauncherConfig = toml::from_str(&contents).map_err(|err|{
        error!("{:?}", err);
        "Failed to deserialize"
    })?;
    Ok(decoded)
}
