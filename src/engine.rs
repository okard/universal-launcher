
use std::collections::HashMap;
use std::process::{Command, Stdio, Child};

use regex::{Regex, Captures};

use ::model;


pub struct Engine
{
    regex_vars: Regex,
    path_vars: HashMap<String, String> // replace with resolver func/ trait?
    // list of running commands

    // channel for communication?
}

impl Default for Engine
{
    fn default() -> Self
    {
        Engine
        {
            regex_vars: Regex::new(r"\$(?P<varname>[[:alpha:]]*)").unwrap(),
            path_vars : HashMap::<String, String>::default()
        }
     }
}

impl Engine
{

    pub fn reg_variable(&mut self, name: &str, value : &str)
    {
        self.path_vars.insert(name.into(), value.into());
    }

    fn populate_string(&self, s: &str) -> String
    {
        // solve variables inside of string params
        let resolved_path = self.regex_vars.replace(s, |caps: &Captures| {
                let varname = caps.name("varname").map_or("", |m| m.as_str());
                //info!("varname: {}", varname);
                let resolved_varname = self.path_vars.get(varname);
                //info!("resolved_varname: {:?}", resolved_varname);
                resolved_varname.cloned().unwrap_or_default()
        });
        resolved_path.into()
    }

    fn spawn_command(&self, exe: &model::Executable) -> Result<Child, String>
    {
        //TODO handle all options

        let resolved_path = self.populate_string(&exe.path);
        // Convert paths?
        info!("run exe {}", resolved_path);
        let command = Command::new(resolved_path)
        .args(exe.args.clone()) //TODO resolved args
        .stdout(Stdio::inherit()) // possible to pipe to socket for the dispatch?
        .stderr(Stdio::inherit())
        .spawn();
        //TODO exe.cwd

        //nix::sys::memfd::memfd_create based buffer which then can be used in event loop to detect changes?
        // windows story?

        command.map_err(|err| format!("{:?}", err))
    }

    // spawn command function -> Result<(i, Child)>

    pub fn run(&self, lc: &model::LauncherConfig)
    {
        // list of the running childs
        let commands = &mut Vec::new();

        // initial startup of commands
        info!("Running {}", lc.general.name);
        for (i, exe) in lc.executables.iter().enumerate()
        {
            match self.spawn_command(exe)
            {
                Ok(c) => commands.push((i, c)),
                Err(err) => error!("Error executing command {:?}", err)
            }
        }

        // watcher loop:
        let restart_commands = &mut Vec::new();
        let mut is_running = true;
        while is_running
        {
            // loop through commands and check state
            for (idx, command) in commands.into_iter()
            {
                let exe_config = &lc.executables[*idx];

                match command.try_wait()
                {
                    Ok(Some(status)) =>
                    {
                        debug!("[{}]: pid {} exited with: {}", exe_config.path, command.id(), status);
                        // restart command when required
                        if exe_config.respawn {
                            //TODO Implement restart delay
                            restart_commands.push((*idx, self.spawn_command(exe_config).unwrap()));
                        }
                    }
                    Ok(None) => debug!("[{}]: pid {} still running", exe_config.path, command.id()),
                    Err(e) => debug!("[{}]: error attempting to wait: {}", exe_config.path, e),
                }
            }

            // move the restartet processes into current process watch list
            debug!("Restart process count: {}", restart_commands.len());
            commands.append(restart_commands);
            // remove dead items
            debug!("Current process count before cleanup: {}", commands.len());
            commands.retain(|(_, ref mut c)| c.try_wait().ok().map_or(false, |s| s.is_none())); // remove items with err or exit status
            debug!("Current process count after cleanup: {}", commands.len());

            is_running = commands.len() > 0;
            // sleep a second and then watch again
            use std::{thread, time};
            thread::sleep(time::Duration::from_millis(if is_running {5000} else {0}));
        }
    }

}
