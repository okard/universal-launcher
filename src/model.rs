
// launcher options

// TODO zero copy model?

#[derive(Serialize, Deserialize, Debug)]
pub struct LauncherConfig
{
    pub general: GeneralInfo,
    pub executables: Vec<Executable>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct GeneralInfo
{
    pub name : String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum ExecutableType
{
    Default, //Spawn and wait for all processes to finish (be parent)

    //forget //just start the the process and forget

    //systray keep it in systray
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum StreamOption
{
    Parent,
    // Log Managed?
    // Socket?
    File {path: String},
    Drop
}

impl Default for StreamOption
{
    fn default() -> Self { StreamOption::Parent }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Executable
{
    // description?

    #[serde(rename = "type")]
    pub exec_type: ExecutableType,
    pub cwd: Option<String>,
    pub path: String,
    pub args: Vec<String>,
    #[serde(default)]
    pub respawn: bool,
    pub respawn_delay_ms: Option<u32>,

    #[serde(default)]
    pub stdout: StreamOption,

    #[serde(default)]
    pub stderr: StreamOption,

    // log options (control stdin/stderr)
        //no
        //stdin/stdout/stderr
        //parent
        //file
}
