# universal launcher application

To handle application which are split in multiple executables, e.g. a static file server + rest backend, or a classical webserver + database engine system.

This launcher uses a toml based config file to start and monitor a set of executables, incl. auto restart on crashes, one running launcher executable should represent one application (so one config file) with its multiple processes

## Status

**WIP**

- [ ] Basic implementation
- [ ] V2.0 Cross Platform SysTray Implementation

## Operating Systems

- Linux/Unix
- Windows
- (Mac)

## Example configs

```toml
[general]
name= ""

[[executable]]
type="default"
path=""
args=[]
respawn=true

```
