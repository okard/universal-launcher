
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate clap;

extern crate launch;

use std::path::Path;
use clap::{App, Arg};


fn main()
{
    //setup logger
    use log::LevelFilter;
    use env_logger::{Builder, WriteStyle};
    let mut builder = Builder::new();
    builder.filter(None, LevelFilter::Trace)
       .write_style(WriteStyle::Always)
       .init();

    // parse cli
    let matches = App::new("ulauncher")
         .version("v0.1-alpha")
         .arg(Arg::with_name("INPUT")
                            .help("Sets the input file to use")
                            .required(true)
                            .index(1))
         .get_matches();

    // get filename
    let launcherfile_path = &match matches.value_of("INPUT")
    {
        Some(value) => Path::new(value),
        None => panic!("Clap should print error when required argument is missing")
    };

    info!("Using input file: {:?}", launcherfile_path);

    // load the config
    let conf = launch::load_file(launcherfile_path);

    // execute it when possible
    match conf {
        Err(s) => eprintln!("{}", s),
        Ok(c) => {
            debug!("{:#?}", c);
            let engine = &mut launch::Engine::default();
            engine.reg_variable("configpath", launcherfile_path.parent().unwrap().to_str().unwrap());
            engine.run(&c);
            // sort of stepped run to react on signals? state<'a>.step(StepOption::Step(SleepTime)|StepOption::Kill)?
        }
    }
}
